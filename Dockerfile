FROM mjenz/ros-kinetic-desktop-full

RUN apt update

# Required instal

RUN apt install -y ros-kinetic-moveit-core
RUN apt install -y ros-kinetic-moveit-ros-planning-interface
RUN apt install -y ros-kinetic-moveit-visual-tools
RUN apt install -y ros-kinetic-moveit-ros-visualization
RUN apt install -y ros-kinetic-arbotix-python
RUN sudo apt -y install ros-kinetic-rtabmap-ros
RUN sudo apt -y install ros-kinetic-kobuki-desktop

# Bonus usage
RUN echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
RUN grep -wq '^source /etc/profile.d/bash_completion.sh' ~/.bashrc || echo 'source /etc/profile.d/bash_completion.sh'>>~/.bashrc


# install turtlebot2i remote view
WORKDIR "/root"
RUN pwd

RUN mkdir -p turtlebot2i_ws/src
WORKDIR "/root/turtlebot2i_ws/src"
RUN pwd

RUN git clone https://github.com/Interbotix/arbotix_ros.git -b turtlebot2i
RUN git clone https://github.com/Interbotix/phantomx_pincher_arm.git
RUN git clone https://github.com/Interbotix/turtlebot2i.git

WORKDIR "/root/turtlebot2i_ws"
RUN pwd
RUN rosdep update
RUN rosdep install --from-paths src --ignore-src -r -y
RUN . /opt/ros/kinetic/setup.sh && \catkin_make
RUN echo "source /root/turtlebot2i_ws/devel/setup.bash" >> ~/.bashrc

WORKDIR "/root"

RUN echo "Setting up Environment variables..."
RUN echo "export TURTLEBOT_3D_SENSOR=astra" >> ~/.bashrc
RUN echo "export TURTLEBOT_3D_SENSOR2=sr300" >> ~/.bashrc
RUN echo "export TURTLEBOT_BATTERY=None" >> ~/.bashrc
RUN echo "export TURTLEBOT_STACKS=interbotix" >> ~/.bashrc
RUN echo "export TURTLEBOT_BASE=kobuki" >> ~/.bashrc
RUN echo "export TURTLEBOT_ARM=pincher" >> ~/.bashrc
RUN echo "\n# Setup ROS network config : " >> ~/.bashrc
RUN echo "export ROS_MASTER_URI=http://turtlebot.local:11311" >> ~/.bashrc
RUN echo 'export ROS_IP=$(echo `hostname -I | cut -d" " -f1`)' >> ~/.bashrc
RUN echo 'if [ -z "$ROS_IP" ]; then\n\texport ROS_IP=127.0.0.1\nfi' >> ~/.bashrc

RUN apt clean
