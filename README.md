# AS_Turtlebot_Interface

Welcome on the Turtlebot interface repository !
In this repository you will find everything you need to have a functional interface with a turtlebot2i (ros kinetic, rviz, moveit...).

## Getting started

Look at the [wiki](https://gitlab.com/as_turtlerace/as_turtlebot_interface/-/wikis/home) to launch the interface.

## Link

Thank's to mjenz for the ros kinetic docker image : https://hub.docker.com/u/mjenz
